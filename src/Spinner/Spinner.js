import React from 'react'
import { useSelector } from 'react-redux'
import { ClockLoader } from 'react-spinners'

export default function Spinner() {
    let isLoading = useSelector(state => state.spinnerReducer.isLoading)

    if (isLoading) {
        return (
            <div className='fixed w-screen h-screen bg-black top-0 left-0 z-10 flex justify-center items-center'>
                <ClockLoader color="#DC0000" size={150} speedMultiplier={0.5} />
            </div>
        )
    }
    else {
        return <></>
    }

}
