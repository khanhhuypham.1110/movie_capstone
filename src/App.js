import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage/HomePage';
import Layout from './HOC/Layout';
import LoginPage from './Pages/LoginPage/LoginPage';
import Spinner from './Spinner/Spinner';
import SigninPage from './Pages/SigninPage/SigninPage';
import OrderPage from './Pages/OrderPage/OrderPage';
import DetailPage from './Pages/DetailPage/DetailPage';
import useWindowDimension from './services/userWindowDimension';
import MaintainPage from './Pages/MaintainPage/MaintainPage';
import AdminPage from './Pages/AdminPage/AdminPage'



function App() {
    const { width, height } = useWindowDimension()

    if (width < 500) {
        return (
            <div className='App'>
                <BrowserRouter>
                    <Routes>
                        <Route path="*" element={<MaintainPage />} />
                    </Routes>
                </BrowserRouter>
            </div>
        )

    } else {
        return (
            <div className="App">
                <div className='bg'></div>
                <Spinner />
                <BrowserRouter>
                    <Routes>
                        <Route path='/' element={
                            <Layout>
                                <HomePage />
                            </Layout>
                        } />
                        <Route path='/signin' element={<SigninPage />} />
                        <Route path="/login" element={
                            <Layout>
                                <LoginPage />
                            </Layout>
                        } />
                        <Route path="/order" element={
                            <Layout>
                                <OrderPage />
                            </Layout>
                        } />
                        <Route path="/detail/:id" element={
                            <Layout>
                                <DetailPage />
                            </Layout>
                        } />

                        <Route path="/admin/user" element={
                            <Layout>
                                <AdminPage />
                            </Layout>
                        } />
                    </Routes>

                </BrowserRouter>
            </div>
        );
    }

}

export default App;
