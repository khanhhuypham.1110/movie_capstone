
export const USER_LOGIN = "USER_LOGIN_MOVIE_REACT"
export const localStorageService = {
    get: () => {
        let userJSON = localStorage.getItem(USER_LOGIN)
        console.log(userJSON);
        if (userJSON !== null) {
            return JSON.parse(userJSON)
        } else {
            return null
        }
    },
    set: (userData) => {
        let userJSON = JSON.stringify(userData)
        localStorage.setItem(USER_LOGIN, userJSON)
    },

    remove: () => { localStorage.removeItem(USER_LOGIN) }
} 