import axios from "axios"
import { setLoadingOff, setLoadingOn } from "../redux/spinnerSlice"
import { store } from "../redux/store"
import { localStorageService } from "./localStorageService"

const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjI4LzA1LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTIzMjAwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1Mzc5NjAwfQ.FtGbsXl4qyqTRfJrunro0mQ7b-tNs8EWbhb7JDTzloE"
const BASE_URL = "https://movienew.cybersoft.edu.vn"

console.log(localStorageService.get()?.accessToken);
const createConfig = () => {
    return {
        TokenCyberSoft: TOKEN,
        Authorization: "Bearer " + localStorageService.get()?.accessToken
    }
}
export const http = axios.create({
    baseURL: BASE_URL,
    headers: createConfig()
})

// console.log(createConfig());

http.interceptors.request.use(function (config) {
    // Do something before request is sent
    store.dispatch(setLoadingOn())
    return config;
}, function (error) {
    // Do something with request error
    store.dispatch(setLoadingOff())
    return Promise.reject(error);
})

http.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    store.dispatch(setLoadingOff())
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    store.dispatch(setLoadingOff())
    return Promise.reject(error);
})