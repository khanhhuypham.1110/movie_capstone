import { http } from "./configURL"

export const adminService = {
    getUserList: () => {
        return http.get("api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01")
    },
    deleteUser: (userId) => {
        return http.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${userId}`)
    },
    putUpdateUser: (updatingInfor) => {
        return http.put(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, updatingInfor)
    }

} 