import { http } from "./configURL"


export const movieService = {
    getMovieList: () => {
        return http.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05")
    },
    getSearchMovie: (keyWord) => {
        return http.get(`api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01&tenPhim=${keyWord}`)
    },

    getMovieListWithPage: (page, pageSize) => {
        return http.get(`/api/QuanLyPhim/LayDanhSachPhimPhanTrang?soTrang=${page}&soPhanTuTrenTrang=${pageSize}`)
    },

    getMovieBanner: () => {
        return http.get("/api/QuanLyPhim/LayDanhSachBanner")
    },

    getTheaterSyst: () => {
        return http.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01")
    },

    getBoxOfficeList: (scheduleId) => {
        return http.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${scheduleId}`)
    },

    getMovieByID: (movieId) => {
        return http.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieId}`)
    },

    getMovieSchedule: (movieId) => {
        return http.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${movieId}`)
    },

    postOrderTicket: (orderData) => {
        console.log(orderData);
        return http.post("/api/QuanLyDatVe/DatVe", orderData)
    }




}