import React from 'react'
import Footer from '../Component/Footer/Footer'
import Header from '../Component/Header/Header'

const backgroundStyle = {
    backgroundImage: `url("./public/img/a.jpg")`,
    minHeight: "100vh",
    backgroundAttachment: "fixed",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
}

export default function Layout({ children }) {
    return (
        <div style={backgroundStyle}>
            <Header className="mb-5" />
            {children}
            <Footer />
        </div>
    )
}

