import React from 'react'
import { motion } from "framer-motion"
import { AiFillAndroid, AiFillApple, AiFillFacebook, AiFillTwitterCircle } from 'react-icons/ai'


export default function FooterTablet() {
    return (
        <footer className='grid grid-cols-3 py-10 px-0  bg-neutral-900 text-white border-t-2 border-neutral-200'>
            <div>
                <h5 className='font-bold text-left'>TIX</h5>
                <div className='grid grid-cols-2 gap-y-1 text-neutral-400 mt-5 font-semibold text-xs'>
                    <div><a className='hover:text-neutral-100 duration-150 cursor-pointer'>FAQ</a></div>
                    <div><a className='hover:text-neutral-100 duration-150 cursor-pointer'>Thỏa thuận sử dụng</a></div>
                    <div><a className='hover:text-neutral-100 duration-150 cursor-pointer'>Grand Guidlines</a></div>
                    <div><a className='hover:text-neutral-100 duration-150 cursor-pointer'>Chính sách bảo mật</a></div>
                </div>
            </div>
            <div className='text-left'>
                <h5 className='font-bold'>Partner</h5>
                <div className='grid grid-cols-4 gap-y-6 mt-5'>
                    {partner.map((p) => {
                        return (
                            <motion.div
                                whileHover={{ scale: 1.1, translateX: 5 }}
                                whileTap={{ scale: 0.95, translateX: -3 }}>
                                <a href={`${p.url}`} target="_blank"><img src={`${p.imgURL}`} className="w-[30px] h-[30px] object-contain rounded-full" alt="" /></a>
                            </motion.div>
                        )
                    })}
                </div>
            </div>
            <div className='col-span-1 mt-0'>
                <div className='grid grid-cols-2'>
                    <div>
                        <h5 className='font-bold text-left'>MOBILE APP</h5>
                        <div className='text-3xl flex justify-start text-neutral-400   space-x-2 mt-5'>
                            <AiFillAndroid />
                            <AiFillApple />
                        </div>
                    </div>
                    <div >
                        <h5 className='font-bold text-left'>SOCIAL</h5>
                        <div className='text-3xl flex justify-start text-neutral-400  space-x-2 mt-5'>
                            <AiFillFacebook />
                            <AiFillTwitterCircle />
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}



let partner = [
    {
        name: "",
        url: "https://www.cgv.vn/",
        imgURL: "https://gigamall.com.vn/data/2019/05/06/11365490_logo-cgv-500x500.jpg",
    },
    {
        name: "",
        url: "https://www.bhdstar.vn/",
        imgURL: "https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/logo.png",
    },
    {
        name: "",
        url: "https://www.galaxycine.vn/",
        imgURL: "https://www.galaxycine.vn/website/images/galaxy-logo.png",
    },

    {
        name: "",
        url: "https://cinestar.com.vn/",
        imgURL: "https://cinestar.com.vn/pictures/400x400.png",
    },
    {
        name: "",
        url: "https://www.megagscinemas.vn/",
        imgURL: "https://www.megagscinemas.vn/images/home/logo.png",
    },
    {
        name: "",
        url: "https://www.betacinemas.vn/home.htm",
        imgURL: "https://www.betacinemas.vn/Assets/Common/logo/logo.png",
    },
    {
        name: "",
        url: "http://ddcinema.vn/",
        imgURL: "http://ddcinema.vn/Content/Img/logo.png",
    },
    {
        name: "",
        url: "https://touchcinema.com/",
        imgURL: "https://touchcinema.com/images/touchcinema.png",
    },
    {
        name: "",
        url: "https://cinemaxvn.com/",
        imgURL: "https://theme.hstatic.net/1000296517/1000449871/14/logo.png?v=5945",
    },

    {
        name: "",
        url: "http://starlight.vn/",
        imgURL: "https://starlight.vn/Content/img/logo.png",
    },
    {
        name: "",
        url: "https://dcine.vn/",
        imgURL: "https://www.dcine.vn/Content/img/logo.png",
    },
    {
        name: "",
        url: "https://zalopay.vn/",
        imgURL: "https://simg.zalopay.com.vn/zlp-website/assets/logo1_ff390716a5.svg",
    },
    {
        name: "",
        url: "https://portal.vietcombank.com.vn/Pages/Home.aspx?devicechannel=default",
        imgURL: "https://admin.tamlyvietphap.vn/uploaded/Images/Original/2020/10/16/logo_vietcombank_1610091313.jpg",
    },
    {
        name: "",
        url: "https://techcombank.com/khach-hang-ca-nhan",
        imgURL: "https://s3-symbol-logo.tradingview.com/techcombank--600.png",
    },
    {
        name: "",
        url: "https://www.indovinabank.com.vn/",
        imgURL: "https://www.indovinabank.com.vn/sites/default/files/ivb_logo_color.svg",
    }
]