import React from 'react'
import { motion } from "framer-motion"
import { AiFillAndroid, AiFillApple, AiFillFacebook, AiFillTwitterCircle } from "react-icons/ai"
import { Desktop, Mobile, Tablet } from '../../HOC/responsive'
import FooterDesktop from './FooterDesktop'
import FooterTablet from './FooterTablet'
import FooterMobile from './FooterMobile'

export default function Footer() {
    return (
        <div>
            <Desktop>
                <FooterDesktop />
            </Desktop>

            <Tablet>
                <FooterTablet />
            </Tablet>

            <Mobile>
                <FooterMobile />
            </Mobile>

        </div>
    )
}


let partner = [
    {
        name: "",
        url: "https://www.cgv.vn/",
        imgURL: "https://gigamall.com.vn/data/2019/05/06/11365490_logo-cgv-500x500.jpg",
    },
    {
        name: "",
        url: "https://www.bhdstar.vn/",
        imgURL: "https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/logo.png",
    },
    {
        name: "",
        url: "https://www.galaxycine.vn/",
        imgURL: "https://www.galaxycine.vn/website/images/galaxy-logo.png",
    },

    {
        name: "",
        url: "https://cinestar.com.vn/",
        imgURL: "https://cinestar.com.vn/pictures/400x400.png",
    },
    {
        name: "",
        url: "https://www.megagscinemas.vn/",
        imgURL: "https://www.megagscinemas.vn/images/home/logo.png",
    },
    {
        name: "",
        url: "https://www.betacinemas.vn/home.htm",
        imgURL: "https://www.betacinemas.vn/Assets/Common/logo/logo.png",
    },
    {
        name: "",
        url: "http://ddcinema.vn/",
        imgURL: "http://ddcinema.vn/Content/Img/logo.png",
    },
    {
        name: "",
        url: "https://touchcinema.com/",
        imgURL: "https://touchcinema.com/images/touchcinema.png",
    },
    {
        name: "",
        url: "https://cinemaxvn.com/",
        imgURL: "https://theme.hstatic.net/1000296517/1000449871/14/logo.png?v=5945",
    },

    {
        name: "",
        url: "http://starlight.vn/",
        imgURL: "https://starlight.vn/Content/img/logo.png",
    },
    {
        name: "",
        url: "https://dcine.vn/",
        imgURL: "https://www.dcine.vn/Content/img/logo.png",
    },
    {
        name: "",
        url: "https://zalopay.vn/",
        imgURL: "https://simg.zalopay.com.vn/zlp-website/assets/logo1_ff390716a5.svg",
    },
    {
        name: "",
        url: "https://portal.vietcombank.com.vn/Pages/Home.aspx?devicechannel=default",
        imgURL: "https://admin.tamlyvietphap.vn/uploaded/Images/Original/2020/10/16/logo_vietcombank_1610091313.jpg",
    },
    {
        name: "",
        url: "https://techcombank.com/khach-hang-ca-nhan",
        imgURL: "https://s3-symbol-logo.tradingview.com/techcombank--600.png",
    },
    {
        name: "",
        url: "https://www.indovinabank.com.vn/",
        imgURL: "https://www.indovinabank.com.vn/sites/default/files/ivb_logo_color.svg",
    }
]