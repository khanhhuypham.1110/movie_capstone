import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localStorageService } from '../../services/localStorageService'
import { MdAppRegistration, MdLogout, MdOutlineLogin } from "react-icons/md";

export default function UserNav() {
    let userInfor = useSelector((state) => state.userReducer.userInfor)
    let handleLogout = () => {
        localStorageService.remove()
        window.location.href = "/login"
    }
    let renderContent = () => {
        if (userInfor !== null) {
            return (
                <div className='text-neutral-50'>
                    <span className='mr-4 font-bold text-lg'>{userInfor.hoTen}</span>
                    <button className=' rounded-lg bg-red-600  px-3 py-1 outline-0' onClick={() => handleLogout()}>Logout</button>
                </div>
            )
        } else {
            return (
                <div className='flex'>
                    <NavLink to="/login" className='px-4 py-1 border-r-2 border-neutral-400 text-neutral-100'>
                        <div className='flex items-center gap-2'>
                            <span ><MdOutlineLogin /></span>
                            <span>
                                Login
                            </span>
                        </div>

                    </NavLink>

                    <NavLink to="/signin" className=' px-4 py-1 text-neutral-100'>
                        <div className='flex items-center gap-2'>
                            <span><MdAppRegistration /></span>
                            <span>Signin</span>
                        </div>

                    </NavLink>

                </div>
            )
        }
    }
    return (
        <div className='space-x-5'>
            {renderContent()}
        </div>
    )

}
