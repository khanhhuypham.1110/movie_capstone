import React from 'react'
import { NavLink } from 'react-router-dom'
import UserNav from './UserNav'

export default function Header() {
    return (
        <div className='flex px-10 py-5 border-b-2 border-red-600 justify-between items-center'>
            <NavLink to="/">
                <span className='text-xl text-red-600 font-bold'>HUY'S CINEMA</span>
            </NavLink>
            <NavLink to="/admin/user">
                <span className='text-xlfont-bold text-white'>Admin page</span>
            </NavLink>
            <UserNav />
        </div>
    )
}
