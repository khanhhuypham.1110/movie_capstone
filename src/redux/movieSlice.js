import { createSlice } from "@reduxjs/toolkit"



const initialState = {
    movieList: {},
    movieSchedule: null
}

export const movieSlice = createSlice({
    name: "movieSlice",
    initialState: initialState,
    reducers: {
        getMovieList: (state, action) => {

        },


        setMovieSchedule: (state, action) => {
            state.movieSchedule = action.payload
        }
    }
})

export const { getMovieList, setMovieSchedule } = movieSlice.actions