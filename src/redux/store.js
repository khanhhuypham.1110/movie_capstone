import { configureStore, createSlice } from "@reduxjs/toolkit";
import { movieSlice } from "./movieSlice";
import { orderSlice } from "./orderSlice";
import { spinnerSlice } from "./spinnerSlice";
import { userSlice } from "./userSlice";
import { userAdminSlice } from "./userAdminSlice"


export const store = configureStore({
    reducer: {
        movieReducer: movieSlice.reducer,
        userReducer: userSlice.reducer,
        spinnerReducer: spinnerSlice.reducer,
        orderReducer: orderSlice.reducer,
        userAdminReducer: userAdminSlice.reducer
    }
})