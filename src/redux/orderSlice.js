import { createSlice } from "@reduxjs/toolkit";
import { localStorageService } from "../services/localStorageService";

const initialState = {
    seatList: null,
    choiceList: [],
    orderOnSuccess: false,
    orderResult: {
        scheduleId: null,
        totalBill: 0,
        ticketList: [],
    },

}

export const orderSlice = createSlice({
    name: "orderSlice",
    initialState: initialState,
    reducers: {
        startSelecting: (state) => {
            openInputBox(state.seatList)
            state.choiceList = []
        },

        cancelSelecting: (state) => {
            closeInputBox(state.seatList)
            state.orderResult = { ...state.orderResult, totalBill: 0, ticketList: [] }
        },

        setSeatList: (state, action) => {
            state.seatList = action.payload
        },

        setScheduleIdForOrderResult: (state, action) => {
            state.orderResult = { ...state.orderResult, scheduleId: action.payload }
        },

        stickToSelect: (state, action) => {
            let seat = action.payload
            state.choiceList = handleStickToSelect(seat, state.choiceList)
            state.orderResult = handleOrderResult(state.choiceList, state.orderResult)
        },
        setOrderOnSuccess: (state, action) => {
            state.orderOnSuccess = action.payload
        }

    }
})

export const { startSelecting, setSeatList, stickToSelect, cancelSelecting, setScheduleIdForOrderResult, setOrderOnSuccess } = orderSlice.actions

/*
this handleOrder function is used to translate element of choiceList array into orderResult object
*/
let handleOrderResult = (choiceList, orderResult) => {
    let totalBill = 0;
    let ticketList = []
    choiceList.forEach((choice) => {
        totalBill += choice.giaVe
        ticketList.push({
            maGhe: choice.maGhe,
            giaVe: choice.giaVe,
            tenGhe: choice.tenGhe
        })
    })
    return { ...orderResult, totalBill: totalBill, ticketList: ticketList }
}


/*
    this handleStickToSelect function is used to selected seat into choiceList array
*/
let handleStickToSelect = (seat, choiceList) => {
    let inputTag = document.getElementById(`${seat.maGhe}`)
    switch (inputTag.checked) {
        case true: {
            choiceList.push(seat)
            break
        }
        case false: {
            let index = choiceList.findIndex((item) => item.maGhe === seat.maGhe)
            choiceList.splice(index, 1)
            break
        }
        default:
            break;
    }
    return choiceList
}

let openInputBox = (seatList) => {
    seatList.forEach((seat) => {
        if (seat.daDat === false) {
            document.getElementById(`${seat.maGhe}`).disabled = false
        }
    })
}

let closeInputBox = (seatList) => {
    seatList.forEach((seat) => {
        if (seat.daDat === false) {
            document.getElementById(`${seat.maGhe}`).disabled = true
            document.getElementById(`${seat.maGhe}`).checked = false
        }

    })

}