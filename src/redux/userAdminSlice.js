import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    updateInfor: null
}

export const userAdminSlice = createSlice({
    name: "userAdminSlice",
    initialState: initialState,
    reducers: {
        setUpdateInfor: (state, action) => {
            console.log(action.payload);
            state.updateInfor = action.payload
        }
    }
})


export const { setUpdateInfor } = userAdminSlice.actions
