import { createSlice } from "@reduxjs/toolkit";
import { localStorageService } from "../services/localStorageService";

const initialState = {
    userInfor: localStorageService.get()
}

export const userSlice = createSlice({
    name: "userSlice",
    initialState: initialState,
    reducers: {
        setUserInfor: (state, action) => {
            state.userInfor = action.payload
        }
    }
})

export const { setUserInfor } = userSlice.actions
