import React from 'react';
import { Button, Form, Input, message, Select } from 'antd';
import { Option } from 'antd/es/mentions';
import { userService } from '../../services/userService';
import { useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import bg_signin from "../../asset/131219-newsletter-widget.json"

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    string: {
        min: "'${label}' must be at least ${min} characters",
    },
    pattern: {
        mismatch: "'${label}' does not match pattern ${pattern}",
    },
};
/* eslint-enable no-template-curly-in-string */
const prefixSelector = (
    <Form.Item name="prefix" noStyle>
        <Select defaultValue="+84"
            style={{
                width: 70,
            }}
        >
            <Option value="84">+84</Option>
        </Select>
    </Form.Item>
);

export default function SigninPage() {
    const navigate = useNavigate()
    const onFinish = (values) => {
        console.log(values);
        userService.Signin(values).then(res => {
            message.success("Register successfully")
            setTimeout(() => {
                navigate('/login')
            }, 1000)
        }).catch(err => {
            console.log(err);
            message.error(err.response.data.content)
        })

    };


    const onFinishedFailed = (err) => {
        console.log(err);
        err.errorFields.forEach((e) => message.error(e.errors))
    }
    return (

        <div className='w-screen h-screen container flex justify-center items-center p-10'>
            <div className='w-1/3'>
                <Lottie animationData={bg_signin} />
            </div>
            <div className='w-2/3 self-start pt-20'>
                <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages} onFinishFailed={onFinishedFailed}>
                    <Form.Item
                        name='taiKhoan'
                        label={<p className='text-neutral-50 font-semibold'>Username</p>}
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name='matKhau'
                        label={<p className='text-neutral-50 font-semibold'>Password</p>}
                        rules={[
                            {
                                required: true,
                            },

                            {
                                type: "string",
                                min: 6
                            }
                        ]}
                        hasFeedback
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        name="confirm"
                        label={<p className='text-neutral-50 font-semibold'>Confirm Password</p>}
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Please confirm your password!',
                            },

                            {
                                type: "string",
                                min: 6,
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('matKhau') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        name='email'
                        label={<p className='text-neutral-50 font-semibold'>Email</p>}
                        rules={[
                            {
                                required: true,
                            },
                            {
                                type: 'email',
                            },

                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name='soDt'
                        label={<p className='text-neutral-50 font-semibold'>Phone Number</p>}
                    >
                        <Input addonBefore={prefixSelector} />
                    </Form.Item>
                    <Form.Item name='maNhom' label={<p className='text-neutral-50 font-semibold'>Group Code</p>}>
                        <Input />
                    </Form.Item>
                    <Form.Item name='hoTen' label={<p className='text-neutral-50 font-semibold'>Full Name</p>}>
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{
                            ...layout.wrapperCol,
                            offset: 8,
                        }}
                    >
                        <Button className='bg-blue-500 text-white' htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form >
            </div>

        </div>
    );
}
