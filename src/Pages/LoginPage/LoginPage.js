import React from 'react'
import { Button, Form, Input } from 'antd';
import { userService } from '../../services/userService';
import { message } from 'antd';
import { localStorageService } from '../../services/localStorageService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUserInfor } from '../../redux/userSlice';
import Lottie from "lottie-react";
import bg_login from "../../asset/cycle-animation.json"

export default function LoginPage() {
    let navigate = useNavigate()
    let dispatch = useDispatch()

    const onFinish = (credential) => {
        userService.Login(credential).then((res) => {
            localStorageService.set(res.data.content)
            dispatch(setUserInfor(res.data.content))
            setTimeout(() => {
                window.location.href = "/"
            }, 1000)
            message.success("login successfully")
        }).catch((err) => {
            onFinishFailed(err)
            message.error(err.response.data.content)
        })

    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
        message.error("fail to login, please try again")
    };
    return (
        <div className='w-screen h-screen flex justify-center'>
            <div className='w-1/3'>
                <Lottie animationData={bg_login}></Lottie>
            </div>
            <div className='w-2/3 pt-48'>
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 8,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label={<p className='text-neutral-50 font-semibold'>username</p>}
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label={<p className='text-neutral-50 font-semibold'>password</p>}
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 10,
                            span: 2,
                        }}
                    >
                        <Button className='bg-blue-500 text-white' htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>

        </div>
    );
}
