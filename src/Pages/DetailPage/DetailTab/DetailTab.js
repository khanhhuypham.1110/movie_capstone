import { Divider, List, Tabs } from 'antd'
import moment from 'moment'
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { NavLink } from 'react-router-dom'
import { setMovieSchedule } from '../../../redux/movieSlice'
import { store } from '../../../redux/store'

export default function DetailTab(props) {
    let systems = props.systems
    return (
        <div>
            <Tabs
                tabPosition='left'
                defaultActiveKey="1"
                style={{ height: 400, paddingLeft: "0px", transform: "translateX(-24px)" }}
                items={
                    systems.map((system) => {
                        return {
                            label: <img src={system.logo} alt="" className='w-16 h-16 object-cover' />,
                            key: system.maHeThongRap,
                            children: renderTheaterBranch(system.cumRapChieu),
                        }
                    })

                }
            />
        </div>
    )
}


let renderTheaterBranch = (listBranch) => {
    return (
        <>
            <Tabs
                tabPosition='left'
                defaultActiveKey="1"
                style={{ height: 400, paddingLeft: "0px", transform: "translateX(-24px)" }}
                items={listBranch.map(branch => {
                    return {
                        label: renderLable(branch),
                        key: branch.maCumRap,
                        children: renderSchedule(branch.lichChieuPhim),
                    }
                })}
            />
        </>
    )
}


let renderLable = (branch) => {
    return (
        <div className='flex items-center space-x-3 sm:w-[250px] md:w-[280px]'>
            < img src={branch.hinhAnh} alt="" className='hidden lg:block w-16 h-16 object-cover rounded-full' />
            <div className='flex-col text-left'>
                <p className='text-green-600 font-semibold'>{branch.tenCumRap}</p>
                <p className='text-neutral-400 font-medium overflow-hidden'>
                    {branch.diaChi.length > 20
                        ?
                        <>
                            <span>{branch.diaChi.split(", ")[0]}</span><br /><span>{branch.diaChi.split(", ")[1]}</span>
                        </>
                        :
                        <span>{branch.diaChi}</span>}
                </p>
            </div>
        </div>
    )
}


let renderSchedule = (scheduleList) => {
    return (
        <div
            id="scrollableDiv"
            style={{
                height: 400,
                width: 250,
                overflow: 'auto',
                padding: '0 0px',
                transform: "translateX(-24px)"
            }}
        >
            <InfiniteScroll
                dataLength={scheduleList.length}
                endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
                scrollableTarget="scrollableDiv"
            >
                <List
                    dataSource={scheduleList}
                    renderItem={(schedule, key) => (
                        <List.Item key={key} >
                            <div className="flex justify-center w-full">
                                <NavLink to={"/order"} onClick={() => { store.dispatch(setMovieSchedule(schedule)) }}>
                                    <div className='bg-neutral-100 border-2 shadow-md px-3 py-2 rounded-sm font-semibold space-x-1 w-[160px]'>
                                        <span className='text-green-600'>{moment(schedule.ngayChieuGioChieu).format("DD-MM-YYYY")}</span>
                                        <span>~</span>
                                        <span className='text-red-600'>{moment(schedule.ngayChieuGioChieu).format("hh:mm")}</span>
                                    </div>
                                </NavLink>
                            </div>
                        </List.Item>
                    )}
                />
            </InfiniteScroll>
        </div>
    )
}