import { Button, Divider, List, Modal, Rate, Skeleton, Tabs } from 'antd'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { useDispatch } from 'react-redux'
import { NavLink, useParams } from 'react-router-dom'
import { setMovieSchedule } from '../../redux/movieSlice'
import { store } from '../../redux/store'
import { movieService } from '../../services/movieService'
import DetailTab from './DetailTab/DetailTab'

export default function DetailPage() {
    let movieId = useParams("id").id
    const [movie, setMovie] = useState({})
    const [schedule, setSchedule] = useState({})
    const [open, setOpen] = useState(false);
    const showModal = () => {
        setOpen(true);
    };
    const handleCancel = () => {
        setOpen(false);
    };

    useEffect(() => {
        movieService.getMovieByID(`${movieId}`)
            .then((res) => {
                setMovie(res.data.content)
            }).catch((err) => {
                console.log(err);
            })
    }, [])

    useEffect(() => {
        movieService.getMovieSchedule(movieId)
            .then((res) => {
                setSchedule(res.data.content)
            }).catch((err) => {
                console.log(err);
            })
    }, [])

    return (
        <>
            {Object.keys(movie).length !== 0 && Object.keys(schedule).length !== 0
                ?
                <div>
                    <div className='flex items-center h-[600px] p-14'>
                        <div className='w-[300px] h-full rounded-lg'>
                            <img className=' h-full object-cover rounded-lg' src={movie.hinhAnh} alt="" />
                        </div>
                        <div className=' text-sm md:text-[16px] font-semibold w-[400px] h-full space-y-5 p-5 text-neutral-400 backdrop-blur-sm'>
                            <Rate value={movie.danhGia} disabled />
                            <p><span className='text-neutral-100 font-bold'>ngày khởi chiếu: </span>{moment(movie.ngayKhoiChieu).format("DD.MMM.YYYY")}</p>
                            <p><span className='text-neutral-100 font-bold'>Tên Phim: </span>{movie.tenPhim}</p>
                            <p><span className='text-neutral-100 font-bold'>Mô tả: </span>{movie.moTa.length > 400 ? movie.moTa.slice(0, 400) + "......" : movie.moTa}</p>
                            <p><span className='text-neutral-100 font-bold'>Thời lượng: </span>120 phút</p>
                            <button className='bg-orange-600 px-3 py-2 rounded-lg text-white outline-none hover:scale-[1.05] active:scale-[1]' onClick={showModal}>Mua vé</button>
                            <a href={`#${movie.maPhim}`} className='block bg-yellow-500 px-3 py-2 rounded-lg text-white outline-none hover:scale-[1.05] active:scale-[1] w-[110px]'>Xem Trailer</a>
                            <Modal
                                width={710}
                                open={open}
                                title="Lịch chiếu phim"
                                onCancel={handleCancel}
                                footer={[
                                    <Button key="back" onClick={handleCancel}>
                                        Return
                                    </Button>,
                                ]}
                            >
                                {schedule.heThongRapChieu.length !== 0
                                    ?
                                    <DetailTab systems={schedule.heThongRapChieu} />
                                    :
                                    "Không có dữ liệu hoặc phim đã ngừng chiếu"
                                }

                            </Modal>
                        </div>
                    </div>

                    <div className='px-10 mb-36 lg:px-56 lg:mb-56'>
                        <iframe id={movie.maPhim} className='w-full h-[500px] border-y-2' src={`//www.youtube.com/embed/${getId(movie.trailer)}`}
                            frameborder="0" allowfullscreen title='trailer' ></iframe >
                    </div>

                </div>
                :
                <></>
            }
        </>
    )
}




function getId(url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
        ? match[2]
        : null;
}


