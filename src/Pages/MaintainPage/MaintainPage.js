import Lottie from "lottie-react";
import React from 'react'
import bg_maintain from "../../asset/maintenance.json"

export default function MaintainPage() {
    return (
        <div className="flex items-center">
            {/* <Lottie animationData={bg_maintain} style={{ height: "100%" }}></Lottie> */}
            <h4 className="absolute top-[50%] left-[10%] text-yellow-400 font-semibold">Hiện tại phiên bản này chưa hỗ trợ trên mobie, bạn hãy đăng nhập bằng máy tính hoặc tablet để sử dụng nhé</h4>
        </div>
    )
}
