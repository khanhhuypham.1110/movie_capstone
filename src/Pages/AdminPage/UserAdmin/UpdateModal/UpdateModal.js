import { Button, Modal } from 'antd';
import React, { useState } from 'react'
import { BsWrench } from 'react-icons/bs';

import UpdateForm from './UpdateForm';

export default function UpdateModal() {
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const showModal = () => {
        setOpen(true);
    };
    const handleOk = () => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            document.getElementById("updateFormBtnId").click()
        }, 1000);
    };
    const handleCancel = () => {
        setOpen(false);
        document.getElementById("clear-btn").click()
    };
    return (
        <>
            <Button onClick={showModal} id="modal-btn" className='hidden'>
            </Button>

            <Modal
                open={open}
                title="Title"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Return
                    </Button>,
                    <Button key="submit" loading={loading} onClick={handleOk} >
                        sumbit
                    </Button>,
                ]}
            >
                <UpdateForm />
            </Modal>
        </>
    );
}
