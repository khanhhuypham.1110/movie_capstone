import React from 'react'
import { Button, Form, Input, message } from 'antd';
import { useSelector } from 'react-redux';
import { adminService } from '../../../../services/adminService';


const layout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 18,
    },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};
/* eslint-enable no-template-curly-in-string */

export default function UpdateForm() {
    const updateInfor = useSelector((state) => state.userAdminReducer.updateInfor)
    const [form] = Form.useForm();
    const initialValues = {
        "taiKhoan": updateInfor.taiKhoan,
        "email": updateInfor.email,
        "hoTen": updateInfor.hoTen,
        "maLoaiNguoiDung": updateInfor.maLoaiNguoiDung,
        "soDT": updateInfor.soDT
    }
    const onFinish = (values) => {
        adminService.putUpdateUser(values).then((res) => {
            console.log(res.data.content);
            message.success("updates Successfully")
        }).catch((err) => {
            console.log(err);
            message.error(err.response.data.content)
        })
    };
    return (
        <Form {...layout} form={form} initialValues={initialValues} onFinish={onFinish} validateMessages={validateMessages} id="updateForm">
            <Form.Item
                name='taiKhoan'
                label="Tài Khoản"
            >
                <Input defaultValue={updateInfor.taiKhoan} />
            </Form.Item>
            <Form.Item
                name='email'
                label="Email"
                rules={[
                    {
                        type: 'email',
                    },
                ]}
            >
                <Input defaultValue={updateInfor.email} />
            </Form.Item>
            <Form.Item
                name='hoTen'
                label="Họ Tên"
            >
                <Input defaultValue={updateInfor.hoTen} />
            </Form.Item>
            <Form.Item name="maLoaiNguoiDung" label="Loại người dùng">
                <Input disabled defaultValue={updateInfor.maLoaiNguoiDung} />
            </Form.Item>
            <Form.Item name="soDT" label="Số Điện thoại">
                <Input defaultValue={updateInfor.soDT} />
            </Form.Item>
            <Form.Item
                wrapperCol={{
                    ...layout.wrapperCol,
                    offset: 8,
                }}
                className="hidden"
            >
                <Button htmlType='submit' id="updateFormBtnId" >
                    Submit
                </Button>
                <Button onClick={() => { form.resetFields(); }} id="clear-btn" >
                    Clear
                </Button>
            </Form.Item>
        </Form>
    );
}
