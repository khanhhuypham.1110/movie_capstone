import { message, Table, Tag } from 'antd'
import React, { useEffect, useState } from 'react'
import { BsFillTrashFill, BsWrench } from 'react-icons/bs'
import { useDispatch } from 'react-redux'
import { setUpdateInfor } from '../../../redux/userAdminSlice'
import { adminService } from '../../../services/adminService'
import MovieAdmin from '../MovieAdmin/MovieDashBoard'
import UpdateModal from './UpdateModal/UpdateModal'



export default function UserAdminPage() {
    const [userArr, setUserArr] = useState([])
    const dispatch = useDispatch()
    useEffect(() => {
        let deleteUser = (userId) => {
            adminService.deleteUser(userId).then((res) => {
                console.log(res.data.content);
                message.success("delete Successfully")
            }).catch(err => {
                message.error(err.response.data.content)
                console.log(err);
            })
        }


        let editUser = (userInfor) => {
            dispatch(setUpdateInfor(userInfor))
            document.getElementById('modal-btn').click()
        }


        let fetchUserList = () => {
            adminService.getUserList().then((res) => {
                let cloneUserArr = res.data.content.map((item) => {
                    return {
                        ...item, action: (
                            <>
                                <button onClick={() => { editUser(item) }} className='p-2 bg-yellow-500 rounded-lg text-neutral-100 text-lg mr-2 outline-0'>
                                    <BsWrench></BsWrench>
                                </button>
                                <button className="p-2 bg-red-500 rounded-lg text-neutral-100 text-lg" onClick={() => { deleteUser(item.taiKhoan) }}><BsFillTrashFill></BsFillTrashFill></button>
                            </>)
                    }
                })
                setUserArr(cloneUserArr)
            }).catch(err => { console.log(err); })
        }
        fetchUserList()

    }, [])
    return (
        <div>
            <Table columns={tableColumn} dataSource={userArr} />
            <UpdateModal />

        </div>
    )
}

const tableColumn = [
    {
        title: "Tài Khoản",
        dataIndex: "taiKhoan",
        key: "taiKhoan"
    },
    {
        title: "Họ tên",
        dataIndex: "hoTen",
        key: "hoTen",
    },
    {
        title: "Email",
        dataIndex: "email",
        key: "email",
    },
    {
        title: "Loại Khách",
        dataIndex: "maLoaiNguoiDung",
        key: "maLoaiNguoiDung",
        render: (text) => {
            if (text === "QuanTri") {
                return <Tag color='red'>Quản Trị</Tag>
            } else if (text === "KhachHang") {
                return <Tag color="blue">Khách Hàng</Tag>
            }
        }
    },
    {
        title: "Thao tác",
        dataIndex: "action",
        key: "action",
    },
]


