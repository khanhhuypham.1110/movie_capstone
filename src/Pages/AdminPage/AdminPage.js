import React, { useState } from 'react';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { AiFillFileZip, AiOutlineUserSwitch } from 'react-icons/ai';

import UserAdminDashBoard from './UserAdmin/UserAdminDashBoard';
import MovieDashBoard from './MovieAdmin/MovieDashBoard';
import AddMovie from './MovieAdmin/AddMovie';



const { Content, Sider } = Layout;


const item = [
    {
        key: "WATCH_USER_DETAIL",
        icon: (<AiOutlineUserSwitch></AiOutlineUserSwitch>),
        label: "users",

    },

    {
        key: "Films",
        icon: (<AiFillFileZip></AiFillFileZip>),
        label: "Films",
        children: [
            {
                key: "WATCH_FILM_DETAIL",
                label: "Films",
            },
            {
                key: "ADD_NEW",
                label: "Add new",
            }
        ]
    }
]


export default function AdminPage() {
    const [selectedMenuItem, setSelectedMenuItem] = useState("item1")
    const menuItemSwitcher = (key) => {
        switch (key) {
            case "WATCH_USER_DETAIL":
                return (<><UserAdminDashBoard /></>)
            case 'WATCH_FILM_DETAIL':
                return (<><MovieDashBoard /></>);
            case 'ADD_NEW':
                return (<><AddMovie /></>);
            default:
                break;
        }
    }


    const { token: { colorBgContainer }, } = theme.useToken();
    return (
        <div>
            <Layout>
                <Sider style={{ background: colorBgContainer, }}>
                    <Menu
                        theme='dark'
                        mode="inline"
                        defaultSelectedKeys={['WATCH_FILM_DETAIL']}
                        defaultOpenKeys={['Films']}
                        style={{
                            height: '100%',
                            borderRight: 0,
                        }}
                        items={item}
                        onClick={(e) => { setSelectedMenuItem(e.key); console.log(e.key); }}
                    />
                </Sider>
                <Layout style={{ padding: '0 24px 24px', }}>
                    <Breadcrumb style={{ margin: '16px 0', }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>
                    <Content
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                            background: colorBgContainer,
                        }}
                    >
                        {menuItemSwitcher(selectedMenuItem)}
                    </Content>
                </Layout>
            </Layout>
        </div>
    );
}
