import React, { useEffect, useState } from 'react';
import { Space, Table, Tag } from 'antd';
import { movieService } from '../../../../services/movieService';
import moment from 'moment';
import { BsFillCalendar2DateFill, BsFillTrashFill, BsPencilSquare } from 'react-icons/bs';
import { useDispatch } from 'react-redux';

export default function TableMovieAdmin() {
    const [movieArr, setMovieArr] = useState([])
    const dispatch = useDispatch()
    useEffect(() => {
        movieService.getMovieList().then((res) => {
            let cloneMovieArr = res.data.content.map((movie) => {
                return {
                    ...movie, action: (
                        <div className='grid grid-cols-3 text-xl'>
                            <button onClick={() => { }} className='p-2 text-orange-600 text-lg outline-0'>
                                <BsPencilSquare></BsPencilSquare>
                            </button>
                            <button className="p-2 rounded-lg text-red-600 text-lg" onClick={() => { }}>
                                <BsFillTrashFill></BsFillTrashFill>
                            </button>
                            <button className='p-2 text-blue-600'>
                                <BsFillCalendar2DateFill></BsFillCalendar2DateFill>
                            </button>
                        </div>)
                }
            })
            setMovieArr(cloneMovieArr)
        }).catch((err) => {
            console.log(err);
        })
    }, [])
    return (
        <div>
            <Table columns={columns} dataSource={movieArr} />
        </div>
    )
}


const columns = [
    {
        title: 'MovieId',
        dataIndex: 'maPhim',
        key: 'maPhim',
        width: '5%',
        render: (text) => <a>{text}</a>,
    },
    {
        title: '',
        dataIndex: 'hinhAnh',
        key: 'hinhAnh',
        width: "10%",
        render: (img) => <div className='w-24 h-36 mx-auto'><img src={img} alt="" className="object-fill " /></div>
    },
    {
        title: 'Movie Title',
        dataIndex: 'tenPhim',
        key: 'tenPhim',
        width: "10%",

    },

    {
        title: 'Description',
        key: 'moTa',
        dataIndex: 'moTa',
        width: "20%",
        render: (desc) => <p>{desc.length >= 150 ? <>{desc.slice(0, 150) + "..."}</> : <>{desc}</>}</p>,

    },
    {
        title: "",
        key: "ngayKhoiChieu",
        dataIndex: "ngayKhoiChieu",
        width: "10%",
        render: (str) => <p className='w-fit'><span>{moment(str).format("DD-MMM-YYYY") + " "}</span>~<span>{" " + moment(str).format("hh-mm")}</span></p>
    },
    {
        title: 'Rating',
        dataIndex: 'danhGia',
        key: 'danhGia',
        width: "5%"
    },
    {
        title: 'Action',
        dataIndex: "action",
        key: 'action',
        width: "10%",
    },
];

