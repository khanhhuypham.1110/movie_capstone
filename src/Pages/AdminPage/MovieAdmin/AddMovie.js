import React from 'react';
import { Button, DatePicker, Form, Input, InputNumber, Switch, Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
const layout = {
    labelCol: {
        span: 4,
    },
    wrapperCol: {
        span: 8,
    },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};
/* eslint-enable no-template-curly-in-string */


export default function AddMovie() {
    const normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e?.fileList;
    };
    const onFinish = (values) => {
        console.log(values);
    };
    return (
        <div>
            <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                <Form.Item
                    name="title"
                    label="Movie Title"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="trailer"
                    label="Trailer"
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"

                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item
                    nam="releaseDate"
                    label="Release Date">
                    <DatePicker />
                </Form.Item>
                <Form.Item
                    label="Now Playing"
                    valuePropName="checked">
                    <Switch />
                </Form.Item>
                <Form.Item
                    label="Upcoming"
                    valuePropName="checked">
                    <Switch />
                </Form.Item>
                <Form.Item
                    label="Top Rated"
                    valuePropName="checked">
                    <Switch />
                </Form.Item>
                <Form.Item
                    name="rating"
                    label="Rating"
                >
                    <InputNumber />
                </Form.Item>

                <Form.Item label="Dragger">
                    <Form.Item name="dragger" valuePropName="fileList" getValueFromEvent={normFile} noStyle>
                        <Upload.Dragger name="files" action="/upload.do">
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                        </Upload.Dragger>
                    </Form.Item>
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        ...layout.wrapperCol,
                        offset: 8,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
