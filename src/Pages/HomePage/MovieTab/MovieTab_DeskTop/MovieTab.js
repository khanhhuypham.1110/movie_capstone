import React from 'react'
import { Tabs } from 'antd';
import MovieTabItem from './MovieTabItem';


export default function MovieTab(props) {
    let renderTheaterBranch = (listBranch) => {

        let renderLable = (branch) => {
            return (
                <div className='flex items-center space-x-3'>
                    < img src={branch.hinhAnh} alt="" className=' w-16 h-16 object-cover rounded-full' />
                    <div className='flex-col text-left'>
                        <p className='text-green-600 font-semibold'>{branch.tenCumRap}</p>
                        <p className='text-neutral-400 font-medium'>
                            {branch.diaChi.length > 20
                                ?
                                <>
                                    <span>{branch.diaChi.split(", ")[0]}</span><br /><span>{branch.diaChi.split(", ")[1]}</span>
                                </>
                                :
                                <span>{branch.diaChi}</span>}
                        </p>
                    </div>
                </div>
            )
        }

        return (
            <>
                <Tabs
                    tabPosition='left'
                    defaultActiveKey="1"
                    style={{
                        height: 600, paddingLeft: "0px",
                        // transform: "translateX(-24px)"
                    }}
                    items={listBranch.map((branch) => {
                        // onchange(branch.maCumRap)
                        return {
                            label: renderLable(branch),
                            key: branch.maCumRap,
                            children: <MovieTabItem movieList={branch.danhSachPhim} />,
                        }
                    })}
                />
            </>
        )

    }

    return (
        <>
            <div className='w-fit mx-auto relative backdrop-blur-xs p-10'>
                <div className='absolute top-0 left-0 w-full h-full bg-neutral-900 z-0 opacity-30'></div>
                <div className='relative z-20 flex-col space-y-10'>
                    <h1 className='text-yellow-500 text-4xl font-bold uppercase text-center'>chọn rạp và thời gian để đặt vé</h1>
                    <Tabs
                        className='border-l-2'
                        tabPosition='left'
                        defaultActiveKey="1"
                        items={props.theaterSys.map((system) => {
                            return {
                                label: <img src={system.logo} alt="" className='w-16 h-16 object-cover' />,
                                key: system.maHeThongRap,
                                children: renderTheaterBranch(system.lstCumRap),
                            }
                        })}
                    />
                </div>
            </div>
        </>
    )
}
