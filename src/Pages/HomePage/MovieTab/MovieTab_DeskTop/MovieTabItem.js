import { Divider, List } from 'antd'
import moment from 'moment/moment';
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { setMovieSchedule, } from '../../../../redux/movieSlice';

export default function MovieTabItem(props) {
    let dispatch = useDispatch()
    let renderMovie = (movie) => {
        return (
            <>
                <div className='flex p-1 w-[500px]'>
                    <div className='w-[30%] flex justify-center'>
                        <img src={movie.hinhAnh} className="w-32 h-full object-cover" alt='' />
                    </div>
                    <div className='w-[70%] flex flex-col items-start'>
                        <div className='mb-5'>
                            <span className='bg-red-600 px-2 py-1 text-white rounded-md mr-2'>C18</span>
                            <span className='text-neutral-800 lg:text-neutral-100 font-semibold text-xl'>{movie.tenPhim}</span>
                        </div>
                        <div className='grid grid-cols-2 gap-3'>
                            {movie.lstLichChieuTheoPhim.slice(0, 5).map((schedule) => {
                                return (
                                    <NavLink to={"/order"} className='bg-neutral-100 border-2 shadow-md px-3 py-2 rounded-sm font-semibold space-x-1'
                                        onClick={() => { dispatch(setMovieSchedule(schedule)) }}
                                    >
                                        <span className='text-green-600'>{moment(schedule.ngayChieuGioChieu).format("DD-MM-YYYY")}</span>
                                        <span>~</span>
                                        <span className='text-red-600'>{moment(schedule.ngayChieuGioChieu).format("hh:mm")}</span>
                                    </NavLink>
                                )
                            })}
                        </div>

                    </div>
                </div>
            </>
        )
    }

    let renderMovieList = (movieList) => {
        return (
            <div
                id="scrollableDiv"
                style={{
                    height: 600,
                    width: 500,
                    overflow: 'auto',
                    padding: '0 0px',
                }}

            >
                <InfiniteScroll
                    dataLength={movieList.length}
                    endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
                    scrollableTarget="scrollableDiv"
                >
                    <List
                        dataSource={movieList}
                        renderItem={(movie, key) => (
                            <List.Item key={key} style={{ paddingLeft: 0, paddingRight: 0 }}>
                                {renderMovie(movie)}
                            </List.Item>
                        )}
                    />
                </InfiniteScroll>
            </div>
        )

    }


    return (
        <>{renderMovieList(props.movieList)}</>
    )
}
