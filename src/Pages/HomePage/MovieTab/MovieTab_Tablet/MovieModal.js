import React, { useState } from 'react';
import { Button, Modal } from 'antd';

import MovieTabItem from '../MovieTab_DeskTop/MovieTabItem';

export default function MovieModal(props) {
    const branch = props.branch
    const [open, setOpen] = useState(false);
    const showModal = () => {
        setOpen(true);
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (
        <>
            <button onClick={showModal} className='flex items-center space-x-3 w-full outline-0 '>
                <div className='flex items-center space-x-3 w-full'>
                    <img src={branch.hinhAnh} alt="" className='block w-16 h-16 object-cover rounded-full' />
                    <div className='flex-col text-left'>
                        <p className='text-green-600 font-semibold'>{branch.tenCumRap}</p>
                        <p className='text-neutral-400 font-medium'>
                            {branch.diaChi.length > 20
                                ?
                                <>
                                    <span>{branch.diaChi.split(", ")[0]}</span><br /><span>{branch.diaChi.split(", ")[1]}</span>
                                </>
                                :
                                <span>{branch.diaChi}</span>}
                        </p>
                    </div>
                </div>
            </button>
            <Modal
                width={542}
                open={open}
                title="Lịch chiếu phim"
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Return
                    </Button>,
                ]}
            >
                <MovieTabItem movieList={branch.danhSachPhim} />
            </Modal>
        </>
    );
}




