import React from 'react'
import { Tabs } from 'antd';
import MovieModal from './MovieModal';


export default function MovieTab_Tablet(props) {
    let renderTheaterBranch = (listBranch) => {
        return (
            <>
                <Tabs
                    tabBarGutter={0}
                    type="card"
                    tabPosition='left'
                    defaultActiveKey="1"
                    style={{ height: 500, width: "100%", paddingLeft: "2px", }}
                    items={listBranch.map((branch) => {
                        return {
                            label: <div className='w-fit'><MovieModal branch={branch} /></div>,
                            key: branch.maCumRap
                        }
                    })}
                />
            </>
        )
    }

    return (
        <>
            <div className='w-fit mx-auto relative backdrop-blur-xs py-10'>
                <div className='absolute top-0 left-0 w-full h-full bg-neutral-900 z-0 opacity-30'></div>
                <h1 className='relative z-20 text-yellow-500 text-2xl sm:text-3xl font-bold uppercase text-center mb-10'>chọn rạp và thời gian để đặt vé</h1>
                <div className='relative z-20 flex justify-center'>
                    <Tabs
                        style={{ height: 500, margin: "0px auto" }}
                        tabBarGutter={0}
                        type="card"
                        tabPosition='left'
                        defaultActiveKey="1"
                        items={props.theaterSys.map((system) => {
                            return {
                                label: <img src={system.logo} alt="" className='w-16 h-16 object-cover' />,
                                key: system.maHeThongRap,
                                children: renderTheaterBranch(system.lstCumRap),
                            }
                        })}
                    />
                </div>
            </div>
        </>
    )
}
