import React from 'react'
// import required modules
import { Swiper, SwiperSlide } from "swiper/react";
import { BsPlayBtn } from "react-icons/bs";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import "./style.css";

// import required modules
import { Pagination, Navigation } from "swiper";
import { NavLink } from 'react-router-dom';

export default function MovieSwiper(props) {
    return (
        <div className='w-full'>
            <>
                <Swiper
                    lazy={true}
                    slidesPerView={1}
                    spaceBetween={0}
                    loop={true}
                    pagination={{
                        clickable: true,
                    }}
                    navigation={true}
                    modules={[Pagination, Navigation]}
                    className="swiper"
                >
                    {props.bannerSwiper.map((banner, i) => {
                        return (
                            <SwiperSlide className='swiper-slide'>
                                <div className='w-full h-full relative'>
                                    <img src={banner.hinhAnh} alt="" className='w-full h-full object-fill' />
                                    <div className='absolute top-[40%] left-[50%] text-7xl text-white opacity-70 hover:opacity-100  hover:font-extrabold duration-500'>
                                        <NavLink to={`/detail/${banner.maPhim}`}>
                                            <BsPlayBtn></BsPlayBtn>
                                        </NavLink>
                                    </div>
                                </div>
                            </SwiperSlide>
                        )
                    })}
                </Swiper>
            </>
        </div>
    )
}
