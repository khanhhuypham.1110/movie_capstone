import React, { useEffect, useState } from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/responsive'
import { movieService } from '../../services/movieService'
import MovieList from './MovieList/MovieList'
import MovieSearch from './MovieSearch/MovieSearch'

import MovieSwiper from './MovieSwiper/MovieSwiper'
import MovieTab from './MovieTab/MovieTab_DeskTop/MovieTab'
import MovieTab_Tablet from './MovieTab/MovieTab_Tablet/MovieTab_Tablet'


export default function HomePage() {
    const [movieList, setMovieList] = useState([])
    const [bannerList, setBannerList] = useState([])
    const [theaterSys, setTheaterSys] = useState([])


    useEffect(() => {
        movieService.getMovieList().then((res) => {
            let newMovieListArr = res.data.content
            setMovieList(newMovieListArr)
        }).catch((err) => { console.log(err) })

        movieService.getMovieBanner().then((res) => {
            let bannerList = res.data.content
            setBannerList(bannerList)
        }).catch((err) => { console.log(err) })

        movieService.getTheaterSyst().then((res) => {
            let theaterSys = res.data.content
            setTheaterSys(theaterSys)
        }).catch((err) => { console.log(err) })
    }, [])

    return (
        <div className='mb-32'>
            <MovieSwiper bannerSwiper={bannerList} />

            <MovieSearch movieList={movieList} />
            <MovieList movieList={movieList} />

            <Desktop>
                <MovieTab theaterSys={theaterSys} />
            </Desktop>

            <Tablet>
                <MovieTab_Tablet theaterSys={theaterSys} />
            </Tablet>

            <Mobile>
                <MovieTab_Tablet theaterSys={theaterSys} />
            </Mobile>

        </div>
    )
}
