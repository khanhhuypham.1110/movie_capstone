import React, { useEffect, useState } from 'react'
import { Select } from 'antd';
import { movieService } from '../../../services/movieService';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setMovieSchedule } from '../../../redux/movieSlice';


/*this component receive movieList from outside, and then when user search for any film,
we search that film from movieList based on users' keyword and save into movies[].
*/
export default function MovieSearch(props) {
    const [movies, setMovies] = useState([])
    const [systemBranch, setSystemBranch] = useState([])
    const [schedules, setSchedules] = useState([])
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const onChangeForSearchSchedule = (value) => {
        let index = schedules.findIndex((schedule) => schedule.maLichChieu === value)
        dispatch(setMovieSchedule(schedules[index]))
        navigate('/order')
    }

    const onChangeForSearchSystem = (value) => {
        let index = systemBranch.findIndex((system) => system.maCumRap === value)
        setSchedules(systemBranch[index].lichChieuPhim)
    }

    const onChangeForSearchMovie = (value) => {
        movieService.getMovieSchedule(value)
            .then((res) => {
                let newArr = res.data.content.heThongRapChieu.map((system) => system.cumRapChieu)
                let cloneSystemBranch = []
                /*this below code snippet use to flat nested array into */
                if (newArr !== null) {
                    newArr.forEach((item) => {
                        item.forEach((i) => {
                            cloneSystemBranch.push(i)
                        })
                    })
                }
                setSystemBranch(cloneSystemBranch)
            }).catch((err) => {
                console.log(err);
            })


    };

    const onSearch = (value) => {
        let searchResult = props.movieList.filter((movie) => {
            let comparedKeyWord = movie.tenPhim.toLowerCase()
            let keyWord = value.toLowerCase()
            return comparedKeyWord.includes(keyWord)
        })
        setMovies(searchResult)
    };

    return (
        <div className='w-full p-5 mt-10 backdrop-blur-lg '>
            <div className='w-[600px] mx-auto'>
                <div className='flex justify-evenly gap-4'>
                    <Select
                        style={{
                            width: 250,
                        }}
                        showSearch
                        placeholder="Search movie"
                        optionFilterProp="children"
                        onChange={onChangeForSearchMovie}
                        onSearch={onSearch}
                        filterOption={(input, option) =>
                            (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                        }
                        options={movies.map((movie) => {
                            return {
                                label: movie.tenPhim,
                                value: movie.maPhim
                            }
                        })}
                    />
                    <Select
                        placeholder="search system"
                        style={{
                            width: 250,
                        }}
                        onChange={onChangeForSearchSystem}
                        options={
                            systemBranch.map((branch) => {
                                return {
                                    label: branch.tenCumRap,
                                    value: branch.maCumRap,
                                }
                            })
                        }
                    />

                    <Select
                        placeholder="search for schedule"
                        style={{
                            width: 250,
                        }}
                        onChange={onChangeForSearchSchedule}
                        options={
                            schedules.map((schedule) => {
                                return {
                                    label: (
                                        <>
                                            <span className='text-green-600'>{moment(schedule.ngayChieuGioChieu).format("DD-MM-YYYY")}</span>
                                            <span>~</span>
                                            <span className='text-red-600'>{moment(schedule.ngayChieuGioChieu).format("hh:mm")}</span>
                                        </>
                                    ),
                                    value: schedule.maLichChieu
                                }
                            })
                        }
                    />
                </div>
            </div>

        </div>
    )
}





