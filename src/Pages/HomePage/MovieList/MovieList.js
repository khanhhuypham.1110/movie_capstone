import { Pagination } from 'antd'
import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import useWindowDimension from '../../../services/userWindowDimension'
import "./MovieList.css"


export default function MovieList(props) {
    let data = props.movieList
    const [pageSize, setPageSize] = useState(8)
    const [minValue, setMinValue] = useState(0)
    const [maxValue, setMaxValue] = useState(pageSize)

    let handleChange = (cursor) => {
        console.log(cursor);
        if (cursor <= 1) {
            setMinValue(0)
            setMaxValue(pageSize)
        } else {
            setMinValue((cursor - 1) * pageSize)
            setMaxValue(cursor * pageSize)

        }
    };

    return (
        <div className='sm:container mx-auto mt-8'>.
            <div className='flex justify-center'>
                <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-10'>
                    {data &&
                        data.length > 0 &&
                        data.slice(minValue, maxValue).map((movie, i) => (

                            <div className='card min-h-[450px] max-w-[250px] rounded-xl'>
                                <img className='w-full h-full object-cover rounded-xl' src={movie.hinhAnh} alt="" />
                                <div className='backdrop'></div>
                                <div className='buy-ticket-section'>
                                    <div className='content flex-col space-y-4'>
                                        <h4 className='title font-semibold text-xl text-white'>
                                            {movie.tenPhim}
                                        </h4>
                                        <p className='desc text-sm text-white font-semibold'>{movie.moTa.length > 100 ? movie.moTa.slice(0, 100) + "......" : movie.moTa}</p>
                                    </div>

                                    <div className='buy-btn-wrapper'>
                                        <NavLink to={`/detail/${movie.maPhim}`} className="buy-btn">Show Detail</NavLink>
                                    </div>
                                </div>


                            </div>
                        ))}

                </div>
            </div>

            <div className='flex justify-center my-10'>
                <Pagination
                    className='text-yellow-500 font-semibold bg-neutral-300 rounded-lg'
                    defaultCurrent={1}
                    onChange={handleChange}
                    total={data.length}
                />
            </div>

        </div >

    );
}

