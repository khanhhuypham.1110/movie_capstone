import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { setOrderOnSuccess } from '../../redux/orderSlice'
import { movieService } from '../../services/movieService'
import SeatSelection from './SeatSelection/SeatSelection'
import SelectionConfirmation from './SelectionConfirm/SelectionConfirmation'

const styleCss = {
    backgroundImage: "url('./img/banner.jpg')",
    backgroundPosition: "top",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
}


export default function OrderPage() {

    //when user click on schedule, movieSchedule variable of redux will be set
    const movieSchedule = useSelector(state => state.movieReducer.movieSchedule)
    const orderOnSuccess = useSelector(state => state.orderReducer.orderOnSuccess)
    const credential = useSelector(state => state.userReducer.userInfor)
    const [boxOffice, setBoxOffice] = useState(null)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    useEffect(() => {
        if (credential === null) {
            navigate("/login")
        }
        //there are some movie having movieSchedule
        if (movieSchedule !== null) {
            dispatch(setOrderOnSuccess(false))
            movieService.getBoxOfficeList(movieSchedule.maLichChieu).then((res) => {
                setBoxOffice(res.data.content)
            }).catch((err) => { console.log(err); })
        }
    }, [orderOnSuccess])

    return (
        <>
            {movieSchedule !== null && boxOffice !== null ?
                <div className='flex justify-around pt-16 mb-20' style={styleCss}>
                    <div className='w-[60%]'>
                        <SeatSelection boxOffice={boxOffice} />
                    </div>
                    <div className='w-[40%]'>
                        <SelectionConfirmation boxOffice={boxOffice} />
                    </div>
                </div>
                :
                <></>
            }
        </>
    )
}
