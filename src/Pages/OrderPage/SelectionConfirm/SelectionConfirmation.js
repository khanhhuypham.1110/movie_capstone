import { Card, message, Popconfirm } from 'antd'
import { TypeIcon } from 'antd/es/message/PurePanel'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setOrderOnSuccess } from '../../../redux/orderSlice'
import { store } from '../../../redux/store'
import { movieService } from '../../../services/movieService'
import "./selectionConfirmation.css"

export default function SelectionConfirmation(props) {
    let movieInfor = props.boxOffice.thongTinPhim
    let orderResult = useSelector(state => state.orderReducer.orderResult)
    return (
        <div className="selectionConfirm w-fit">
            <Card
                bordered={false}
                className="p-2 h-screen"
            >
                <div className='confirm-card'>
                    <div>
                        <span className='mr-1'>{orderResult.totalBill + " VND"}</span>
                    </div>
                    <div>
                        <span>Theater:</span><span>{movieInfor.tenCumRap}</span>
                    </div>
                    <div >
                        <span>Address:</span><span>{movieInfor.diaChi}</span>
                    </div>
                    <div >
                        <span>box office:</span><span>{movieInfor.tenRap}</span>
                    </div>
                    <div >
                        <span>Schedule:</span><span>{movieInfor.ngayChieu}-{movieInfor.gioChieu}</span>
                    </div>
                    <div>
                        <span>Movie Title:</span><span>{movieInfor.tenPhim}</span>
                    </div>
                    <div>
                        <span>Selection:</span>
                        <span>{
                            orderResult.ticketList.map((item, i) => {
                                return `${item.tenGhe}${orderResult.ticketList.length > ++i ? ", " : ""}`
                            })
                        }</span>
                    </div>
                </div>
                <div className='confirm-btn'>
                    {popupConfirmation(orderResult)}
                </div>

            </Card>
        </div>
    )
}


function confirmOrder(order) {

    let cloneOrderResult = {
        "maLichChieu": order.scheduleId,
        "danhSachVe": order.ticketList.map((seat) => {
            return {
                "maGhe": seat.maGhe,
                "giaVe": seat.giaVe,
            }
        })
    }
    console.log(cloneOrderResult);
    movieService.postOrderTicket(cloneOrderResult).then((res) => {
        message.success("order successfully")
        store.dispatch(setOrderOnSuccess(true))
        document.getElementById('cancel-btn').click()
        console.log(res.data.content);
    }).catch((err) => {
        console.log(err);
        message.error(err.response.data.content)
        document.getElementById('cancel-btn').click()
    })
}


function popupConfirmation(order) {
    const print = () => {
        if (order.ticketList.length <= 0) {
            return (
                <p>There is no selection, please choose at least 1</p>
            )

        } else {
            return (
                <>
                    <div>
                        <p><span>Total bill: </span><span>{order.totalBill}</span></p>
                        {order.ticketList.map((seat) => {
                            return (
                                <>
                                    <p className='font-bold'><span>{seat.tenGhe} :</span><span className='text-green-600'>{seat.giaVe}</span></p>
                                </>
                            )
                        })}
                    </div>
                </>
            )
        }

    }

    const confirm = () => {
        if (order.ticketList.length <= 0) {
            return
        }
        setTimeout(() => { confirmOrder(order) }, 2000)
    }
    return (
        <Popconfirm
            placement="leftTop"
            className=' text-red-500'
            title="Confirmation"
            description={print(order)}
            onConfirm={confirm}
            okText={<p className='text-neutral-900'>ok</p>}

        >
            <button >confirm</button>
        </Popconfirm >
    );
}