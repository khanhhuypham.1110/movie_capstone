import React from 'react';
import { BsSquareFill } from 'react-icons/bs';
import { useDispatch } from 'react-redux';
import { cancelSelecting, setScheduleIdForOrderResult, setSeatList, startSelecting, stickToSelect } from '../../../redux/orderSlice';
import "./SeatSelection.css"

export default function SeatSelection(props) {
    const dispatch = useDispatch()
    let seatListForDisplay = processDataForDisplay(props.boxOffice.danhSachGhe)
    let seatList = props.boxOffice.danhSachGhe //this data is used for redux in order to mainly manipulate data 
    dispatch(setSeatList(seatList)) //send data list up to redux
    dispatch(setScheduleIdForOrderResult(props.boxOffice.thongTinPhim.maLichChieu))// send scheduleId up to redux

    function renderSeat(row) {
        return (
            row.map((seat, index) => {
                return (
                    <td key={index}>
                        {
                            seat.daDat === false
                                ?
                                <>
                                    {seat.loaiGhe === "Thuong"
                                        ?
                                        <>
                                            <input type="checkbox" id={seat.maGhe} onChange={() => { dispatch(stickToSelect(seat)) }} className="seat" disabled="disabled" />
                                            {/* <p for={seat.maGhe} className="absolute top-[50%] left-[50%] text-sm font-semibold">{`${seat.tenGhe}`}</p> */}
                                        </>
                                        :
                                        <input type="checkbox" id={seat.maGhe} onChange={() => { dispatch(stickToSelect(seat)) }} className="seat premiumSeat" disabled="disabled" />
                                    }
                                </>

                                :
                                <input type="checkbox" id={seat.maGhe} onChange={() => { dispatch(stickToSelect(seat)) }} className="seat selectedSeat" disabled="disabled" />

                        }

                    </td>
                )
            })
        )
    }

    function renderRow(list) {
        return (
            list.map((row, index) => {
                return (
                    <tr key={index} className="">
                        {renderSeat(row)}
                    </tr>
                )
            })
        )

    }

    return (
        <div className='selecting-section'>
            <div className='button-section'>
                <div>
                    <button className='btn mr-10' onClick={() => { dispatch(startSelecting()) }}>Start Selecting</button>
                    <button id="cancel-btn" className='btn' onClick={() => { dispatch(cancelSelecting()) }}>Cancel Selecting</button>
                </div>
                <div className='grid grid-cols-2 gap-2 text-sm lg:grid-cols-4 lg:text-lg'>
                    <span className='flex items-center gap-2'>
                        <BsSquareFill className='text-green-600 text-3xl rounded-md' />
                        <span>Selected Seat</span>
                    </span>
                    <span className='flex items-center gap-2'>
                        <BsSquareFill className="text-red-600 text-3xl rounded-md" />
                        <span>Reserved Seat</span>
                    </span>
                    <span className='flex items-center gap-2'>
                        <BsSquareFill className="text-yellow-300 border-orange-500 border-4 rounded-md text-3xl" />
                        <span>Preimum Seat</span>
                    </span>
                    <span className='flex items-center gap-2'>
                        <BsSquareFill className="text-white border-orange-500 border-4 rounded-md text-3xl" />
                        <span>Empty Seat</span>
                    </span>
                </div>
            </div>

            <div className='seat-section'>
                <table>
                    {renderRow(seatListForDisplay)}
                    <tfoot>
                        <tr>
                            <td colSpan="16" className='screen'>
                                <p>SCREEN THIS WAY</p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    );
}


/*
this function is used for creating new array which is only for display purpose
*/
function processDataForDisplay(list) {
    let newArray = []
    let offset = 0
    let sliceSize = 16
    for (let i = 0; i < list.length; i += 16) {
        newArray.push(list.slice(offset, sliceSize))
        offset += 16
        sliceSize += 16
    }
    return newArray
}
